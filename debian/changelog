xgalaga (2.1.1.0-6) unstable; urgency=medium

  * Team upload.
  * Remove obsolete menu file.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Use canonical VCS URI.
  * Fix FTCBFS: Pass --host to ./configure.
    Thanks to Helmut Grohne for the patch. (Closes: #945871)

 -- Markus Koschany <apo@debian.org>  Mon, 01 Jun 2020 14:38:08 +0200

xgalaga (2.1.1.0-5) unstable; urgency=medium

  * Team upload.
  * Use compat level 9 and require debhelper >= 9.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 3.9.6.
  * Add missing Vcs-Browser field and use canonical Vcs-URI. (Closes: #708418)
  * Drop the removal of the old highscore file in postinst because Lenny and
    earlier releases are not supported anymore.
  * Add 0004-clang-ftbfs.patch and fix FTBFS with the clang compiler because
    void functions should not have a returned value.
    Thanks to Arthur Marble for the report and patch. (Closes: #739926)
  * Use copyright format 1.0.
  * debian/rules: Provide hardening build flags.
  * Add 0005-reproducible-build.patch and disable timestamps when using gzip.

 -- Markus Koschany <apo@debian.org>  Sat, 12 Dec 2015 20:56:17 +0100

xgalaga (2.1.1.0-4) unstable; urgency=low

  * Team upload.

  [ Paul Wise ]
  * Drop use of dh_desktop, it does nothing now

  [ Stephen M. Webb ]
  * Switch to 3.0 (quilt) source format.
  * Bumped Standards-Version to 3.9.2 (no changes required).
    - ran wrap-and-sort.
  * Converted copyright file to DEP5 format.
  * debian/rules: added build-arch and build-indep targets.
  * Fixed FTBFS with ld --as-needed (Closes: #641293) (LP: #832918).
  * Converted previous source mods to quilt format.
  * Fixed build warnings due to implicit declarations.
  * Fixed build warnings due to deprecated xf86dga.h header.

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Fri, 09 Dec 2011 23:54:28 -0500

xgalaga (2.1.1.0-3) unstable; urgency=low

  * New maintainer (Closes: #487472), thanks Joey Hess!
    - Add the Debian Games Team as maintainer, myself as uploader
  * Build-Depend on x11proto-core-dev instead of the dummy x-dev package
  * Add freedesktop menu files, tweaked from Ubuntu ones (Closes: #432398)
  * Use the uscan magic URL in the watch file for the QA sf.net redirector
  * Remove the old high score file on upgrade instead of purge
  * Hack SOUNDDEFS to use /usr/lib, drop sndsrv symlink, clean all sndsrvs
    - This also fixes the kFreeBSD FTBFS (Closes: #486113)
  * Give the CREDITS file correct permissions, add symlink in /usr/share/doc
  * Drop conflict on suidmanager as it was removed from Debian in 2003!
  * Drop empty and unused /var/games directory from the package
  * Honour noopt, nostrip, parallel=n in DEB_BUILD_OPTIONS
    - this brings us into line with policy, update Standards-Version
  * Build with more GCC warnings turned on

 -- Paul Wise <pabs@debian.org>  Sat, 05 Jul 2008 00:20:01 +0800

xgalaga (2.1.1.0-2) unstable; urgency=low

  * Orphaned the package.

 -- Joey Hess <joeyh@debian.org>  Sat, 21 Jun 2008 21:39:19 -0400

xgalaga (2.1.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Sun, 09 Mar 2008 18:24:15 -0400

xgalaga (2.1.0.2-2) unstable; urgency=low

  * Fix ftbfs. Closes: #468792

 -- Joey Hess <joeyh@debian.org>  Sat, 01 Mar 2008 13:01:56 -0500

xgalaga (2.1.0.2-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Tue, 26 Feb 2008 18:32:42 -0500

xgalaga (2.1.0-1-1) unstable; urgency=low

  * New upstream release!
    - Includes all patches made to the Debian package over the past ten years.
    - Fixes sound disabling key. Closes: #149743
    - Check return code when writing to /dev/dsp, which fixes problems when
      the sound device is not available. Closes: #60266
    - Limit -level to playable levels. Closes: #399987
  * Added a Homepage field.
  * Remove README.Debian, no longer relevant.
  * Added a watch file.
  * Updated the man page, removed undocumented options.
  * Revert upstream's addition of O_NONBLOCK when opening /dev/dsp, breaks
    sound in my tests.

 -- Joey Hess <joeyh@debian.org>  Fri, 08 Feb 2008 18:47:45 -0500

xgalaga (2.0.34-44) unstable; urgency=low

  * Update menu file for new policy and de-hardcode paths.

 -- Joey Hess <joeyh@debian.org>  Sun, 08 Jul 2007 16:47:29 -0400

xgalaga (2.0.34-43) unstable; urgency=low

  * Take config.sub and config.guess from autotools-dev.
  * kFreeBSD porting. Closes: #415664
  * Create a xgal.sndsrv.oss that will work on linux and, presumably, FreeBSD
    (untested). The source file is left at xgal.sndsrv.linux.c since renaming
    source files in a Debian diff is too ugly.

 -- Joey Hess <joeyh@debian.org>  Mon, 14 May 2007 18:29:49 -0400

xgalaga (2.0.34-42) unstable; urgency=low

  * Fix formatting of --help. Closes: #399985

 -- Joey Hess <joeyh@debian.org>  Thu,  8 Feb 2007 18:31:16 -0500

xgalaga (2.0.34-41) unstable; urgency=low

  * No change upload to get buildds to rebuild it now that
    linux-kernel-headers is fixed. Closes: #389324

 -- Joey Hess <joeyh@debian.org>  Tue,  3 Oct 2006 15:34:13 -0400

xgalaga (2.0.34-40) unstable; urgency=low

  * Fix a fencepost error in gecos name retreival code spotted by
    Thue Janus Kristensen. Closes: #386718

 -- Joey Hess <joeyh@debian.org>  Sat,  9 Sep 2006 17:54:15 -0400

xgalaga (2.0.34-39) unstable; urgency=low

  * Revert last change, this was a linux-kernel-headers bug.

 -- Joey Hess <joeyh@debian.org>  Thu,  7 Sep 2006 21:50:19 -0400

xgalaga (2.0.34-38) unstable; urgency=low

  * Include linux/types.h in linux-joystick.c to avoid a FTBFS when including
    linux/joystick.h with the latest gcc.

 -- Joey Hess <joeyh@debian.org>  Thu,  7 Sep 2006 21:35:49 -0400

xgalaga (2.0.34-37) unstable; urgency=low

  * Patch from Hans de Goede to make window size configurable at run time.
    This includes fixes for the behavior in #369016 when running at
    nonstandard window sizes, it also sets the default window size to 468x586
    again.
  * Finally removed the old xgal compatability symlink to xgalaga.

 -- Joey Hess <joeyh@debian.org>  Wed, 31 May 2006 19:38:58 -0400

xgalaga (2.0.34-36) unstable; urgency=low

  * Set window size back to 400x500 which makes fullscreen not quite that but
    avoids gameplay and display issues from changing the size. Closes: #369016

 -- Joey Hess <joeyh@debian.org>  Fri, 26 May 2006 16:31:50 -0400

xgalaga (2.0.34-35) unstable; urgency=low

  * Patch from Hans de Goede <j.w.r.degoede@hhs.nl> of Fedora Extras to remove
    all the build warnings.
  * Patch from Hans de Goede to add fullscreen support. Note that full screen
    mode is enabled by default.
  * Update man page to document fullscreen support.
  * Moved a lot of stuff to /usr/share.

 -- Joey Hess <joeyh@debian.org>  Wed, 24 May 2006 18:13:41 -0400

xgalaga (2.0.34-34) unstable; urgency=low

  * Fix FTBFS due to xorg transition causing $x_libraries not to be set.
    Closes: #365160

 -- Joey Hess <joeyh@debian.org>  Fri, 28 Apr 2006 13:42:47 -0400

xgalaga (2.0.34-33) unstable; urgency=low

  * Minor improvement to man page. Closes: #349647

 -- Joey Hess <joeyh@debian.org>  Tue, 24 Jan 2006 13:03:39 -0500

xgalaga (2.0.34-32) unstable; urgency=low

  * Debhelper v5.
  * Move away from xlibs-dev.
  * Current policy.

 -- Joey Hess <joeyh@debian.org>  Wed, 30 Nov 2005 20:35:07 -0500

xgalaga (2.0.34-31) unstable; urgency=low

  * Build with -O2 not -O3 so it's debuggable.
  * Add NO_GLOBAL_SCORES define that rips out all support for global score
    file. The old global score file is not deleted until xgalaga is purged,
    but it will not be used anymore.
  * Remove insecure sgid bit. Closes: #319686

 -- Joey Hess <joeyh@debian.org>  Sat, 23 Jul 2005 20:18:10 -0400

xgalaga (2.0.34-30) unstable; urgency=low

  * Patch from Andreas Jochens to support building with gcc 4.
    Closes: #300374

 -- Joey Hess <joeyh@debian.org>  Sat, 19 Mar 2005 11:47:18 -0500

xgalaga (2.0.34-29) unstable; urgency=low

  * Add support for building a xgalaga-hyperspace binary,
    based on a patch by Paul Telford. Closes: #280975

 -- Joey Hess <joeyh@debian.org>  Fri, 12 Nov 2004 18:02:02 -0500

xgalaga (2.0.34-28) unstable; urgency=low

  * Fix non-POSIX chown call in postinst. Closes: #236524
  * And one in the rules file.
  * Quote all strings in menu file.

 -- Joey Hess <joeyh@debian.org>  Sat,  6 Mar 2004 12:44:45 -0900

xgalaga (2.0.34-27) unstable; urgency=low

  * Make the sound server deal with any buffer size the sound card returns.
    Thanks to Chris Capoccia for helping to track this down. See #196979,
    which is not all fixed, since larger buffer sizes make xgalagas sound
    effects lose sync.

 -- Joey Hess <joeyh@debian.org>  Sun, 15 Jun 2003 19:09:43 -0400

xgalaga (2.0.34-26) unstable; urgency=low

  * Joystick patch fix for when two buttons are pressed and one released.

 -- Joey Hess <joeyh@debian.org>  Fri,  6 Jun 2003 16:24:14 -0400

xgalaga (2.0.34-25) unstable; urgency=low

  * Joystick support patch from Wouter Verhelst <wouter@debian.org>, also
    sent upstream. Closes: #190241
  * Add README.Debian with info about the joystick patch. Also update man
    page.
  * Fixed icon to use Debian icon color map.

 -- Joey Hess <joeyh@debian.org>  Thu, 24 Apr 2003 12:28:24 -0400

xgalaga (2.0.34-24) unstable; urgency=low

  * Updated config.sub and config.guess so they know about
    parisc64-unknown-linux-gnu host system type. Closes: #174786

 -- Joey Hess <joeyh@debian.org>  Mon,  3 Feb 2003 14:06:53 -0500

xgalaga (2.0.34-23) unstable; urgency=medium

  * More paranioa: When writing the ~/.xgalscores, first drop the games gid
    so the program opens the file with only the perms of the user who ran it,
    to protect against numerous attacks (like setting HOME, or using a
    symlink..) that let the caller trick xgalaga into overwriting any file
    writable by group games.

 -- Joey Hess <joeyh@debian.org>  Sun, 29 Dec 2002 21:25:35 -0500

xgalaga (2.0.34-22) unstable; urgency=low

  * Passing xgalaga a huge HOME variable could crash it. Patch from Steve Kemp
    to make it use snprintf. Closes: #174624

 -- Joey Hess <joeyh@debian.org>  Sun, 29 Dec 2002 12:29:56 -0500

xgalaga (2.0.34-21) unstable; urgency=low

  * If a huge initial level was given, read_level() would recurse backwards
    looking for a real level. After a few hundred recusions, it would overflow
    the stack of course. I don't think this is exploitable, but I have changed
    it to use a loop looking backward for the level so there is no stack
    overflow. It will still take a _very_ long time to start from level
    11111111111 or so. Closes: #154291

 -- Joey Hess <joeyh@debian.org>  Thu, 25 Jul 2002 17:50:53 -0400

xgalaga (2.0.34-20) unstable; urgency=low

  * Debhelper v4.
  * Fixed man page section.

 -- Joey Hess <joeyh@debian.org>  Thu, 13 Jun 2002 17:25:54 -0400

xgalaga (2.0.34-19) unstable; urgency=low

  * make k a signed char in xgal.sndsrv.linux.c; chars do not default to
    signed on all arches.

 -- Joey Hess <joeyh@debian.org>  Fri, 28 Dec 2001 23:07:07 -0500

xgalaga (2.0.34-18) unstable; urgency=low

  * Makefile.in: if there is no sound server, don't try to install one.
    For the Hurd. Closes: #107995

 -- Joey Hess <joeyh@debian.org>  Wed,  8 Aug 2001 15:28:25 -0400

xgalaga (2.0.34-17) unstable; urgency=low

  * printf is a macro in gcc 3.0, which broke an internally #ifdef'd printf
    (sigh). Patched.
  * Updated config.* files for PARISC, Closes: #97379

 -- Joey Hess <joeyh@debian.org>  Mon, 14 May 2001 15:36:05 -0400

xgalaga (2.0.34-16) unstable; urgency=low

  * Fixed three bugs relating to pausing the program, patch from
    Rune Broberg <mihtjel@myplace.dk>, Closes: #89254
    Also sent upstream.
  * (This upload slightly delayed by accident.)

 -- Joey Hess <joeyh@debian.org>  Sun, 11 Mar 2001 15:21:18 -0800

xgalaga (2.0.34-15) unstable; urgency=low

  * No changes, Closes: #92689 (which I long ago fixed in CVS, but didn't
    consider RC..)

 -- Joey Hess <joeyh@debian.org>  Tue,  3 Apr 2001 13:30:03 -0700

xgalaga (2.0.34-14) unstable; urgency=low

  * statoverride transition

 -- Joey Hess <joeyh@debian.org>  Wed, 10 Jan 2001 15:09:20 -0800

xgalaga (2.0.34-13) unstable; urgency=low

  * Added a menu icon, Closes: #75767
  * The icon is by Robert Cleaver Ancell <neutronium@clear.net.nz>, and is GPL.

 -- Joey Hess <joeyh@debian.org>  Fri,  5 Jan 2001 17:38:18 -0800

xgalaga (2.0.34-12) unstable; urgency=low

  * Made -nosound mode not try to open the sound device. Closes: #78734
    Fix sent upstream.

 -- Joey Hess <joeyh@debian.org>  Thu,  4 Jan 2001 16:08:40 -0800

xgalaga (2.0.34-11) unstable; urgency=low

  * Updated to fhs /var/games.
  * Remove score file on purge.

 -- Joey Hess <joeyh@debian.org>  Sun,  5 Nov 2000 23:05:39 -0800

xgalaga (2.0.34-10) unstable; urgency=low

  * Build deps.

 -- Joey Hess <joeyh@debian.org>  Sat,  4 Dec 1999 18:24:58 -0800

xgalaga (2.0.34-9) unstable; urgency=low

  * README, debian/copyright, title.c: Home page change, Closes: #51569
  * debian/changelog, debian/control: removed 'master'
  * libsprite/data.c, libsprite/data.h, libsprite/defs.h, libsprite/init.c:
    Patch from Kevin Ryde <user42@zip.com.au> to fix the numlock problem.
    Closes: #50903

 -- Joey Hess <joeyh@debian.org>  Mon, 29 Nov 1999 15:03:32 -0800

xgalaga (2.0.34-8) unstable; urgency=low

  * Patch from Kevin Ryde <user42@zip.com.au>, to improve smoothness and
    speed in xgalaga, especially on slower video cards.

 -- Joey Hess <joeyh@debian.org>  Tue,  5 Oct 1999 12:34:03 -0700

xgalaga (2.0.34-7) unstable; urgency=low

  * FHS

 -- Joey Hess <joeyh@debian.org>  Mon,  6 Sep 1999 18:41:48 -0700

xgalaga (2.0.34-6) unstable; urgency=low

  * Modified the sound server to just exit if there is no sound support.
    xgalaga seems to handle this ok now, without crashing as it used to. The
    sound server was sleeping and blocking the SIGTERM signal that was supposed
    to kill it, resulting in it not quitting when the game quit. (#37165)

 -- Joey Hess <joeyh@debian.org>  Tue,  4 May 1999 16:26:32 -0700

xgalaga (2.0.34-5) unstable; urgency=low

  * Actually xgalaga doesn't accept -f at all so removed it from man page.

 -- Joey Hess <joeyh@debian.org>  Mon, 28 Dec 1998 19:42:18 -0800

xgalaga (2.0.34-4) unstable; urgency=low

  * xgal.6x: no they arn't fishes, they are enemy spaceships. (#31203)

 -- Joey Hess <joeyh@debian.org>  Sun, 27 Dec 1998 23:20:57 -0500

xgalaga (2.0.34-3) unstable; urgency=low

  * glibc 2.1 strdup fix. (#29542).

 -- Joey Hess <joeyh@debian.org>  Mon, 16 Nov 1998 12:12:07 -0800

xgalaga (2.0.34-2) unstable; urgency=low

  * Rebuilt with latest debhelper to change how sgid binary is registered.

 -- Joey Hess <joeyh@debian.org>  Tue, 27 Oct 1998 22:29:42 -0800

xgalaga (2.0.34-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Mon, 11 May 1998 08:16:08 -0700

xgalaga (2.0-1) unstable; urgency=low

  * New upstream release.
    - fixes bug #11296, now works on systems w/o sound support.
  * Covered by the GPL now, it is now free!

 -- Joey Hess <joeyh@debian.org>  Thu, 30 Apr 1998 00:32:39 -0700

xgalaga (1.6c-11) unstable; urgency=low

  * Moved xgal.sndsrv.linux to /usr/lib/games/xgalaga/, so I don't need to
    write a man page for it.
  * Moved xgalaga to /usr/games, and it's man page to /usr/man/man6.

 -- Joey Hess <joeyh@debian.org>  Sat, 21 Feb 1998 23:13:30 -0800

xgalaga (1.6c-10) unstable; urgency=low

  * Rebuilt with debhelper 0.60 to fix mode 444 files.
  * Updated standards-version.

 -- Joey Hess <joeyh@debian.org>  Mon,  9 Feb 1998 13:08:56 -0800

xgalaga (1.6c-9) unstable; urgency=low

  * Use debhelper.

 -- Joey Hess <joeyh@debian.org>  Sat,  7 Feb 1998 20:26:30 -0800

xgalaga (1.6c-8) unstable; urgency=low

  * Rebuilt to use xpm4g (#12942).

 -- Joey Hess <joeyh@debian.org>  Sun, 14 Sep 1997 20:12:17 -0400

xgalaga (1.6c-7) unstable; urgency=low

  * New maintainer.
  * Changed it to use my generic debian/rules file.
  * Changed the description.
  * Updated url to home page in copyright file, and trimmed out some other
    non-copyright info from that file (same info was duplicated in the
    README). Added libsprite copyright info. Updated author's mailing
    address.
  * Got rid of shlibs.local.
  * Added a menu file.

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Sep 1997 15:31:17 -0400

xgalaga (1.6c-6) non-free; urgency=low

  * Rebuilt with libc6.
  * Orphaned the package.
  * Cosmetic change (fix bug #9675).

 -- Vincent Renardias <vincent@waw.com>  Fri, 9 May 1997 14:41:58 +0200

xgalaga (1.6c-5) non-free; urgency=low

  * Fixed problem with soundfiles (Bug #6991).
  * Removed empty directory /var/lib/games (Bug #6992).
  * Changed some src code to get rid of compilation warnings.

 -- Vincent Renardias <vincent@waw.com>  Sat, 1 Feb 1997 18:01:05 +0100

xgalaga (1.6c-4) non-free; urgency=low

  * Wrote manpage (Corrects Bug #5593)

 -- Vincent Renardias <vincent@waw.com>  Tue, 17 Dec 1996 23:19:13 +0100

xgalaga (1.6c-3) non-free; urgency=low

  * Changed location and mode of scorefile (Bug #5876)

 -- Vincent Renardias <vincent@waw.com>  Tue, 10 Dec 1996 23:33:33 +0100

xgalaga (1.6c-2) non-free; urgency=low

  * Maintainer changed
  * No man page: added link to undocumented.7
  * Tuned compile options

 -- Vincent Renardias <vincent@waw.com>  Sat, 19 Oct 1996 04:49:33 +0200

xgalaga (1.6c-1) non-free; urgency=low

  * Initial Release

 -- Christoph Lameter <clameter@waterf.org>  Thu, 01 Sep 1996 15:37:25 +0100
